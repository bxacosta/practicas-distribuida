package com.distribuida.rmi.test;

import com.distribuida.rmi.config.RmiClientConfig;
import com.distribuida.rmi.servicios.ServicioOperaciones;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

public class RmiSpringClientMain {

    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(RmiClientConfig.class);

        ServicioOperaciones operaciones = context.getBean(ServicioOperaciones.class);

        System.out.println("Servicio: " + operaciones.getClass());

        int respuesta = operaciones.sumar(4, 3);

        System.out.println("Respuesta: " + respuesta);
    }
}
