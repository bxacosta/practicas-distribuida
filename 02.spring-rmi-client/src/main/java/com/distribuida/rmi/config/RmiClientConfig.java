package com.distribuida.rmi.config;

import com.distribuida.rmi.servicios.ServicioOperaciones;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

@Configuration
public class RmiClientConfig {

    public static final String SERVICE_NAME = "rmi://localhost:1099/Operaciones";

    @Bean
    public RmiProxyFactoryBean operaciones() {
        RmiProxyFactoryBean proxy = new RmiProxyFactoryBean();

        proxy.setServiceUrl(SERVICE_NAME);
        proxy.setServiceInterface(ServicioOperaciones.class);

        return proxy;
    }
}
