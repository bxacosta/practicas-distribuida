package com.distribuida.mail.services;

import com.distribuida.mail.model.Singer;

import java.util.List;

public interface SingerService {

    List<Singer> findAll();
}
