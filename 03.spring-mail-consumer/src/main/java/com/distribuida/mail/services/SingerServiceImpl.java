package com.distribuida.mail.services;

import com.distribuida.mail.model.Album;
import com.distribuida.mail.model.Singer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SingerServiceImpl implements SingerService{

    @Override
    public List<Singer> findAll() {

        // Esto deberia cargarse desde la base de datos
        List<Album> albums1 = new ArrayList<>();
        albums1.add(new Album(1, "Chicha mix", new Date()));
        albums1.add(new Album(2, "Cumbia mix", new Date()));
        albums1.add(new Album(3, "Shungo power", new Date()));

        List<Album> albums2 = new ArrayList<>();
        albums2.add(new Album(1, "Mi historia de amor", new Date()));
        albums2.add(new Album(2, "El mas querido", new Date()));


        List<Singer> singers = new ArrayList<>();
        singers.add(new Singer(1, "Alex", "Coro", new Date(), albums1));
        singers.add(new Singer(2, "Andres", "Veles", new Date(), albums2));
        singers.add(new Singer(3, "Papi", "Juan", new Date(), albums1));


        return singers;
    }
}
