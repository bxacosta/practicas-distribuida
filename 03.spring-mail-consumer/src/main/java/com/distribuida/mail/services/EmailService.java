package com.distribuida.mail.services;

import com.distribuida.mail.model.Singer;

import javax.mail.MessagingException;
import java.util.List;

public interface EmailService {

    void sendEmail(String to, String subject, List<Singer> singers) throws MessagingException;
}
