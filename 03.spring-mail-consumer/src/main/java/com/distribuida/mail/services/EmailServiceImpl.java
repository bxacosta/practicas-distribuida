package com.distribuida.mail.services;

import com.distribuida.mail.model.Album;
import com.distribuida.mail.model.Singer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.List;

@Component
public class EmailServiceImpl implements EmailService {

    @Autowired
    public JavaMailSender emailSender;

    @Override
    public void sendEmail(String to, String subject, List<Singer> singers) throws MessagingException {

        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "utf-8");

        String htmlMessage = "";

        mimeMessage.setContent(buildHtmlMessage(singers), "text/html");
        message.setTo(to);
        message.setSubject(subject);

        emailSender.send(mimeMessage);
    }

    private String buildHtmlMessage(List<Singer> singers) {
        String message = "<h2>Reporte de cantantes</h2>";
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        for (Singer singer : singers) {
            message = message +
                    "<p style='margin-bottom: 0; margin-top: .2rem;'><strong>Id: </strong>"+ singer.getId() +"</p>" +
                    "<p style='margin-bottom: 0; margin-top: .2rem;'><strong>First Name: </strong>"+ singer.getFirstName() +"</p>" +
                    "<p style='margin-bottom: 0; margin-top: .2rem;'><strong>Last Name: </strong>"+ singer.getLastName() +"</p>" +
                    "<p style='margin-bottom: 0; margin-top: .2rem;'><strong>Birth Date: </strong>"+ df.format(singer.getBirthDate()) +"</p>" +
                    "<p style='margin-bottom: 0; margin-top: .2rem;'><strong>Albumes: </strong></p>" +
                    "<ul>";

            for (Album album : singer.getAlbums()) {
                message = message + "<li>" +
                        "<strong>Id: </strong>" + album.getId() +
                        "<strong> Title: </strong>" + album.getTitle() +
                        "<strong> Release Date: </strong>" + df.format(album.getReleaseDate()) +
                        "</li>";
            }
            message = message + "</ul><br/>";
        }


        return message;
    }
}
