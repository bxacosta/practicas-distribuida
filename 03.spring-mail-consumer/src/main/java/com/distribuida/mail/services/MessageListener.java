package com.distribuida.mail.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.mail.MessagingException;

@Component("messageListener")
public class MessageListener {

    @Autowired
    private EmailService emailService;

    @Autowired
    private SingerService singerService;

    @JmsListener(destination = "mail", containerFactory = "jmsListenerContainerFactory")
    public void onMessage(Message message) throws JMSException, MessagingException {

        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;

            String mail = textMessage.getText();

            System.out.println(">>> Sending mail to: " + mail);
            emailService.sendEmail(mail, "Reporte", singerService.findAll());
        } else {
            System.out.println("Message is not a instance of TextMessage");
        }
    }
}
