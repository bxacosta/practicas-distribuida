package com.distribuida.mail.test;

import com.distribuida.mail.config.ArtemisConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

public class ArtemisConsumerMain {


    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.distribuida.mail.config");
        System.out.println("Consumer Listening...");

        System.in.read();
        context.close();
    }
}
