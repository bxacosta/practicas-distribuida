package com.distribuida.rmi.servicios;

import org.springframework.stereotype.Component;

@Component
public class ServicioOperacionsImpl implements ServicioOperaciones {

    @Override
    public int sumar(int n1, int n2) {
        return n1 + n2;
    }
}
