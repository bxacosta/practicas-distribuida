package com.distribuida.rmi.config;

import com.distribuida.rmi.servicios.ServicioOperaciones;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;

@Configuration
@ComponentScan(basePackages = "com.distribuida.rmi.servicios")
public class RmiServerConfig {

    @Bean(name = "operacionsRmi")
    public RmiServiceExporter operaciones(ServicioOperaciones servicioOperaciones) {

        RmiServiceExporter exporter = new RmiServiceExporter();

        exporter.setServiceName("Operaciones");
        exporter.setService(servicioOperaciones);
        exporter.setServiceInterface(ServicioOperaciones.class);

        return exporter;
    }
}
