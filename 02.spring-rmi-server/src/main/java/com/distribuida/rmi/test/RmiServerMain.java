package com.distribuida.rmi.test;

import com.distribuida.rmi.config.RmiServerConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

public class RmiServerMain {

    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(RmiServerConfig.class);

        System.out.println("Servidor RMI Iniciado...");
        System.in.read();

        context.close();
    }
}
