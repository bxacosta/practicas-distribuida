package com.distribuida.test;

import com.distribuida.config.EjemploConfig;
import com.distribuida.servicios.ServicioOperaciones;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestContenedorMain {

    public static void main (String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(EjemploConfig.class);
        ServicioOperaciones servicio = ctx.getBean(ServicioOperaciones.class);
        int r = servicio.sumar(2,3);

        System.out.println("Resultado: "+ r);
        ctx.close();
    }
}
