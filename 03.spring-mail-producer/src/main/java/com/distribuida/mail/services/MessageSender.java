package com.distribuida.mail.services;

public interface MessageSender {

    void sendMessage(String message);

}
