package com.distribuida.mail.controllers;

import com.distribuida.mail.services.MessageSender;
import org.springframework.context.ApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class IndexController {

    private String mail;

    @PostConstruct
    public void init() {
        System.out.println("@PostConstruct IndexController");
    }

    public void enviar() {
        ApplicationContext ctx = FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance());
        MessageSender messageSender = ctx.getBean(MessageSender.class);

        messageSender.sendMessage(mail);
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
